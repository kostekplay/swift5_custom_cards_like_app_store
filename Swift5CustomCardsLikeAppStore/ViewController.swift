////  ViewController.swift
//  Swift5CustomCardsLikeAppStore
//
//  Created on 20/10/2020.
//  
//

import UIKit
import Cards

class ViewController: UIViewController {
    
    private let card: CardHighlight = {
        let c = CardHighlight(frame: .zero)
        c.backgroundImage = UIImage(named: "background")
        c.icon = UIImage(named: "icon")
        c.title = "Beat \nMy \nHigh Score"
        c.itemTitle = "My 8-bit game"
        c.itemSubtitle = "Item title subtitle"
        c.shadowBlur = 20
        c.buttonText = "GET"
        c.textColor = .white
        c.itemTitleSize = 18
        return c
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        card.frame = CGRect(x: 0,
                            y: view.safeAreaInsets.top,
                            width: view.frame.size.width,
                            height: view.frame.size.width)
        
        view.addSubview(card)
    }
    
    
}

